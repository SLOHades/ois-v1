import java.util.*;

public class Gravitacija{
    
    public static void main(String[] args){
        Scanner s = new Scanner(System.in);
        
        double v = s.nextDouble();
        
        double a = Izracun(v);
        Izpis(v,a);
    }
    
    public static double Izracun(double v){
        double C = 6.674 * Math.pow(10,-11);
        double M = 5.972 * Math.pow(10,24);
        double r = 6.371 * Math.pow(10,6);
        
        double a = (C*M)/Math.pow((r+v),2);
        
        return a;
    }
    
    public static void Izpis(double v,double a){
        System.out.println("Nadmorksa visina: " + v + " , gravitacijski pospesek: " + a);
    }
}